﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileDrop.Models;

namespace FileDrop.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        
        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index(string mode)
        {
            DeleteExpiredFiles();

            Dictionary<string, int> dictionary = new Dictionary<string, int>
            {
                { "10 Minutes", 0 },
                { "30 Minutes", 1 },
                { "1 Hour", 2 },
                { "6 Hours", 3 },
                { "12 Hours", 4 },
                { "1 Day", 5 },
                { "1 Week", 6 },
                { "2 Weeks", 7 },
                { "1 Month", 8 }
            };

            if (!String.IsNullOrWhiteSpace(mode) && mode == "manage")
            {
                dictionary.Add("Never", 9);
            }
            
            return View(dictionary);
        }

        [HttpPost]
        public JsonResult Upload(string desc, int date)
        {
            MethodResponse mr = new MethodResponse();

            try
            {
                List<FileType> fileTypes = _context.FileTypes.ToList();

                Guid guid = Guid.NewGuid();
                string groupid = Convert.ToBase64String(guid.ToByteArray()).Replace("\\", "").Replace("?", "").Replace("'", "").Replace(" ", "").Replace("=", "");
                List<string> filesInvalidFileType = new List<string>();
                List<string> filesExceedingSize = new List<string>();

                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];

                    if (!IsFileTypeAllowed(fileTypes, file))
                    {
                        filesInvalidFileType.Add(fileName);
                    }
                    if ((file.ContentLength / 1024) > (2150000)) // Roughly 2.05GB
                    {
                        filesExceedingSize.Add(fileName);
                    }
                }

                if (filesInvalidFileType.Count > 0 || filesExceedingSize.Count > 0)
                {
                    string html = "";

                    if (filesInvalidFileType.Count > 0)
                    {
                        html += "The following file" + (filesInvalidFileType.Count > 1 ? "s are" : " is") + " not one of the supported file types:";
                        foreach (string file in filesInvalidFileType)
                        {
                            html += "<br />" + file;
                        } 
                    }
                    if (filesExceedingSize.Count > 0)
                    {
                        if (!String.IsNullOrWhiteSpace(html))
                        {
                            html += "<br /><br />";
                        }

                        html += "The following file" + (filesExceedingSize.Count > 1 ? "s are" : " is") + " above the file size limit of 2GB:";
                        foreach (string file in filesExceedingSize)
                        {
                            html += "<br />" + file;
                        }

                        html += "<br /><br />";
                        html += "Please check and try again!";
                    }                    

                    // Return back to user with files not allowed
                    mr.ID = -3;
                    mr.Message = html;
                }
                else
                {
                    // Proceed with saving
                    foreach (string fileName in Request.Files)
                    {
                        HttpPostedFileBase file = Request.Files[fileName];
                        
                        if (String.IsNullOrWhiteSpace(desc))
                        {
                            desc = "Untitled";
                        }

                        DroppedFile df = new DroppedFile
                        {
                            DateUploaded = DateTime.Now,
                            DateToBeDeleted = GetDateToBeDeleted(date),
                            Description = desc,
                            FileType = file.ContentType,
                            OriginalFileName = fileName,
                            GroupID = groupid,
                            FileSize = ByteSizeLib.ByteSize.FromBytes(file.ContentLength).ToString()
                        };

                        df = _context.Files.Add(df);

                        _context.SaveChanges();

                        file.SaveAs(Server.MapPath("~/App_Data/Files/") + df.Id);                        
                    }

                    mr.ID = 0;
                    mr.Message = "Success - " + Request.Files.Count + " files uploaded successfully";
                }                
            }
            catch (Exception ex)
            {
                mr.ID = -1;
                mr.Message = ex.Message;
            }
            
            return Json(mr);
        }
        
        private DateTime GetDateToBeDeleted(int option)
        {
            DateTime dt = DateTime.Now;

            switch (option)
            {
                
                case 0:
                    dt = dt.AddMinutes(10);
                    break;
                case 1:
                    dt = dt.AddMinutes(30);
                    break;
                case 2:
                    dt = dt.AddHours(1);
                    break;
                case 3:
                    dt = dt.AddHours(6);
                    break;
                case 4:
                    dt = dt.AddHours(12);
                    break;
                case 5:
                    dt = dt.AddDays(1);
                    break;
                case 7:
                    dt = dt.AddDays(14);
                    break;
                case 8:
                    dt = dt.AddMonths(1);
                    break;
                case 9:
                    dt = dt.AddYears(10);
                    break;
                case -1:
                case 6:
                default:
                    dt = dt.AddDays(7);
                    break;
            }

            return dt;
        }

        private bool IsFileTypeAllowed(List<FileType> fileTypes, HttpPostedFileBase file)
        {
            int lastDot = file.FileName.LastIndexOf('.');

            if (fileTypes.Exists(z => z.MimeType == file.ContentType) && lastDot > 0)
            {
                string ext = file.FileName.Substring(lastDot).ToLower();
                if (fileTypes.Exists(z => z.Extension == ext))
                {
                    return true;
                }
            }
            return false;
        }

        public ActionResult FileTypes(string mode = "basic")
        {
            DeleteExpiredFiles();

            ViewBag.Mode = mode;
            return View();
        }

        [HttpPost]
        public JsonResult AddFileType(string ext, string mime)
        {
            MethodResponse mr = new MethodResponse();

            if (!String.IsNullOrWhiteSpace(ext) && !String.IsNullOrWhiteSpace(mime))
            {
                string ext1 = ext.StartsWith(".") ? ext : "." + ext;

                // First check this doesn't already exist
                List<FileType> tmpList = _context.FileTypes.Where(z => z.Extension == ext1)
                                                           .Where(z => z.MimeType == mime)
                                                           .ToList();

                if (tmpList.Count == 0)
                {
                    FileType ft = new FileType
                    {
                        Extension = ext1,
                        MimeType = mime
                    };

                    _context.FileTypes.Add(ft);
                    _context.SaveChanges();

                    mr.ID = 0;
                    mr.Message = "File Type Added Successfully!";
                }
                else
                {
                    mr.ID = -3;
                    mr.Message = "";
                }
            }
            else
            {
                mr.ID = -2;
                mr.Message = "Please enter a value for both Extension and Mime Type to continue";
            }

            return Json(mr);
        }

        [HttpPost]
        public JsonResult RemoveFileType(string id)
        {
            MethodResponse mr = new MethodResponse();

            if (!String.IsNullOrWhiteSpace(id))
            {
                int ftID = Convert.ToInt32(id);

                // First check this id actually exists
                FileType ft = _context.FileTypes.SingleOrDefault(z => z.ID == ftID);

                if (ft != null)
                {
                    _context.FileTypes.Remove(ft);
                    _context.SaveChanges();

                    mr.ID = 0;
                    mr.Message = "File Type Removed Successfully!";
                }
                else
                {
                    mr.ID = -3;
                    mr.Message = "File Type doesn't exist!";
                }
            }
            else
            {
                mr.ID = -2;
                mr.Message = "Please enter an ID to continue";
            }

            return Json(mr);
        }

        [HttpPost]
        public JsonResult GetFileTypes()
        {
            return Json(_context.FileTypes.OrderBy(z => z.Extension).ToList());
        }
        
        private void DeleteExpiredFiles()
        {
            DateTime now = DateTime.Now;
            List<DroppedFile> files = _context.Files.Where(z => z.DateToBeDeleted < now).ToList();

            foreach (DroppedFile df in files)
            {
                System.IO.File.Delete(Server.MapPath("~/App_Data/Files/") + df.Id);
                _context.Files.Remove(df);
            }

            _context.SaveChanges();
        }

        public ActionResult DroppedFiles(string sort)
        {
            DeleteExpiredFiles();
            
            List<DroppedFile> files = _context.Files.ToList();

            List<FileGroupViewModel> groups = new List<FileGroupViewModel>();

            foreach (DroppedFile df in files)
            {
                if (!groups.Exists(z => z.GroupID == df.GroupID))
                {
                    // GroupID needs adding
                    groups.Add(new FileGroupViewModel
                    {
                        GroupID = df.GroupID,
                        DateToBeDeleted = df.DateString,
                        DateUploaded = df.DateUploaded.ToString("dd/MM/yyyy HH:mm:ss"),
                        Description = df.Description,
                        Files = new List<DroppedFile>()
                    });
                }
                groups.Single(z => z.GroupID == df.GroupID).Files.Add(df);
            }

            List<SortOptions> options = new List<SortOptions>
            {
                new SortOptions
                {
                    Name = "Deleted Asc",
                    Current = false,
                    URL = "/DroppedFiles?sort=deleted_asc",
                    ID = 3
                },
                new SortOptions
                {
                    Name = "Deleted Desc",
                    Current = false,
                    URL = "/DroppedFiles?sort=deleted_desc",
                    ID = 4
                },
                new SortOptions
                {
                    Name = "Uploaded Asc",
                    Current = false,
                    URL = "/DroppedFiles?sort=uploaded_asc",
                    ID = 5
                },
                new SortOptions
                {
                    Name = "Uploaded Desc",
                    Current = false,
                    URL = "/DroppedFiles?sort=uploaded_desc",
                    ID = 6
                },
                new SortOptions
                {
                    Name = "Name Desc",
                    Current = false,
                    URL = "/DroppedFiles?sort=name_desc",
                    ID = 2
                },
                new SortOptions
                {
                    Name = "Default - Name Asc",
                    Current = false,
                    URL = "/DroppedFiles",
                    ID = 1
                }
            };

            switch (sort)
            {
                case "deleted_asc":
                    groups = groups.OrderBy(z => z.DateToBeDeleted).ToList();
                    options.Single(z => z.Name == "Deleted Asc").Current = true;
                    break;
                case "deleted_desc":
                    groups = groups.OrderByDescending(z => z.DateToBeDeleted).ToList();
                    options.Single(z => z.Name == "Deleted Desc").Current = true;
                    break;
                case "uploaded_asc":
                    groups = groups.OrderBy(z => z.DateUploaded).ToList();
                    options.Single(z => z.Name == "Uploaded Asc").Current = true;
                    break;
                case "uploaded_desc":
                    groups = groups.OrderByDescending(z => z.DateUploaded).ToList();
                    options.Single(z => z.Name == "Uploaded Desc").Current = true;
                    break;
                case "name_desc":
                    groups = groups.OrderByDescending(z => z.Description).ToList();
                    options.Single(z => z.Name == "Name Desc").Current = true;
                    break;
                default:
                    groups = groups.OrderBy(z => z.Description).ToList();
                    options.Single(z => z.Name == "Default - Name Asc").Current = true;
                    break;
            }

            ViewData["SortOptions"] = options.OrderBy(z => z.ID).ToList();

            return View(groups);
        }

        public ActionResult DownloadFile(int fileID)
        {
            if (fileID != -100)
            {
                // Check if that ID is still stored
                DroppedFile df = _context.Files.SingleOrDefault(z => z.Id == fileID);

                if (df != null)
                {
                    try
                    {
                        string path = Server.MapPath("~/App_Data/Files/");
                        byte[] bytes = System.IO.File.ReadAllBytes(path + fileID);
                        return File(bytes, df.FileType, df.OriginalFileName);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Reason = "Unable to download file - " + ex.Message;
                        return View("Error");
                    }
                }
                else
                {
                    ViewBag.Reason = "File not found";
                    return View("Error");
                }
            }
            else
            {
                string path = Server.MapPath("~/App_Data/Files/");
                byte[] bytes = System.IO.File.ReadAllBytes(path + "winrar.exe");
                return File(bytes, "application/octet-stream", "winrar.exe");
            }
        }



        public class MethodResponse
        {
            public int ID { get; set; }
            public string Message { get; set; }

            public MethodResponse()
            {
                ID = -5;
                Message = "Something went wrong. Please try again!";
            }
        }

        
    }
}