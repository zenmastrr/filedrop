﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileDrop.Models
{
    public class DroppedFile
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public DateTime DateUploaded { get; set; }

        public DateTime? DateToBeDeleted { get; set; }

        public string FileType { get; set; }

        public string OriginalFileName { get; set; }
        
        public string GroupID { get; set; }

        public string DateString
        {
            get
            {
                if (DateToBeDeleted.HasValue || DateToBeDeleted != null)
                {
                    return DateToBeDeleted.Value.ToString("dd/MM/yyyy HH:mm:ss");
                }
                else
                {
                    return "never";
                }
            }
        }

        public string FileSize { get; set; }
    }
    
    public class FileGroupViewModel
    {
        public string GroupID { get; set; }
        public string DateUploaded { get; set; }
        public string DateToBeDeleted { get; set; }
        public string Description { get; set; }
        public List<DroppedFile> Files { get; set; }
    }

    public class SortOptions
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Current { get; set; }
        public string URL { get; set; }
    }
}