﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileDrop.Models
{
    public class FileType
    {
        public int ID { get; set; }

        public string Extension { get; set; }

        public string MimeType { get; set; }
    }
}