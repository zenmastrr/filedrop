namespace FileDrop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFileSizeProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DroppedFiles", "FileSize", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DroppedFiles", "FileSize");
        }
    }
}
