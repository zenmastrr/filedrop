// <auto-generated />
namespace FileDrop.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CreateFileTypesModel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateFileTypesModel));
        
        string IMigrationMetadata.Id
        {
            get { return "201802082140468_CreateFileTypesModel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
