namespace FileDrop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGroupIDToDroppedFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DroppedFiles", "GroupID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DroppedFiles", "GroupID");
        }
    }
}
