namespace FileDrop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedModelNameandProps : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DroppedFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        DateUploaded = c.DateTime(nullable: false),
                        DateToBeDeleted = c.DateTime(),
                        FileType = c.String(),
                        OriginalFileName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.FileDrops");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FileDrops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        DateUploaded = c.DateTime(nullable: false),
                        DateToBeDeleted = c.DateTime(nullable: false),
                        ExpiresIn = c.String(),
                        FileType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.DroppedFiles");
        }
    }
}
