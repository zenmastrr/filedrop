namespace FileDrop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateFileTypesModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileDrops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        DateUploaded = c.DateTime(nullable: false),
                        DateToBeDeleted = c.DateTime(nullable: false),
                        ExpiresIn = c.String(),
                        FileType = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FileTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Extension = c.String(),
                        MimeType = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FileTypes");
            DropTable("dbo.FileDrops");
        }
    }
}
