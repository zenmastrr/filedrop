﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FileDrop.Startup))]
namespace FileDrop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
